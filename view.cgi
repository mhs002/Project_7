#!/home/mohammad/Desktop/anaconda-3/bin/python3

print('Content-type: text/html\n\r\n\r\n')

from header_creator import header_creater
from footer_creator import footer_creater
import sys
import mysql.connector 
import cgitb;cgitb.enable()
import cgi

# Connects sql to the script and creates a cursor.
connect = mysql.connector.connect(user="riham",password="Riham_1998",database="my_project")
curs = connect.cursor()

# Imports paremeter transefered
parameter_transfered = cgi.FieldStorage()
parameter_obtained = parameter_transfered.getvalue('id')

print("""
<html>
  <head>
    <title>View Message</title>
  </head>
  <body>
    """)

@header_creater
def header():
    pass
header("View Message")

# Check if the id is an integer
try:
    parameter_obtained = int(parameter_obtained)
except:
    print("<p>The id entered is invalid.</p>")
    sys.exit()

# Obtains the column where the if is from 
text_obtained = "SELECT * FROM messages WHERE id={}".format(parameter_obtained)
curs.execute(text_obtained)
all_the_info = curs.fetchall()
connect.commit()

print("""<p>ID: {}</p>
<p>Sender: {}</p>
<p>Subject: {}</p>
<p>Text: {}</p>
</html>""".format(all_the_info[0][0],all_the_info[0][2],all_the_info[0][1],all_the_info[0][4]))

# Links to either get back to main page or go to the edit page.
print("""<p><a href="main.cgi"><button>Press this to get back to the main page</button></a><p>""")
print("""<p><a href="edit.cgi?reply_to={}"><button>Press this to get go to the edit page</button></a><p>""".format(all_the_info[0][3]))

@footer_creater
def footer():
    pass
footer("Created By Mohammad Shaalan")
